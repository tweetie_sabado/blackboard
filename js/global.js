$(function students() {
		
		$("a.edit-students").on("click", function(event){
			event.preventDefault();
            $('#editStudent').modal('show');


		$.ajax({
		url:'superadmin/retStud',
			  type: 'post',
			  data: {'stud_num': $(this).data('stud_num')},
			  dataType: "json",
			  success: function(data, status) {
				  $('#ustudnum').val(data.stud_num);
				  $('#uspassword').val(data.password);
				  $('#usfname').val(data.fname);
				  $('#uslname').val(data.lname);
				  $('#usbdate').val(data.bdate);
				  $('#usaddress').val(data.address);
				  $('#uscnum').val(data.cnum);

			  
			  },
			  error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			  }
      });
	
	});
	
});

$(function professors() {
		
		$("a.edit-profs").on("click", function(event){
			event.preventDefault();
            $('#editProf').modal('show');


		$.ajax({
		url:'superadmin/retProf',
			  type: 'post',
			  data: {'prof_num': $(this).data('prof_num')},
			  dataType: "json",
			  success: function(data, status) {
				  $('#uprofnum').val(data.prof_num);
				  $('#upfname').val(data.fname);
				  $('#uplname').val(data.lname);
				  $('#upbdate').val(data.bdate);
				  $('#upaddress').val(data.address);
				  $('#upcnum').val(data.cnum);
				  $('#uppassword').val(data.prof_pass);

			  
			  },
			  error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			  }
      });
	
	});
	
});

$(function subjects() {
		
		$("a.edit-subj").on("click", function(event){
			event.preventDefault();
            $('#editSubj').modal('show');


		$.ajax({
		url:'superadmin/retSubj',
			  type: 'post',
			  data: {'subj_code': $(this).data('subj_code')},
			  dataType: "json",
			  success: function(data, status) {
				  $('#usubjcode').val(data.subj_code);
				  $('#uname').val(data.description);
				  $('#uunits').val(data.units);
				  $('#ucprofnum').val(data.prof_num);
				  $('#uroom').val(data.room);
				  $('#usched').val(data.sched);

			  
			  },
			  error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			  }
      });
	
	});
	
});

$(function studsubjects() {
		
		$("a.edit-studsubj").on("click", function(event){
			event.preventDefault();
            $('#editStudsubj').modal('show');


		$.ajax({
		url:'superadmin/retStudsub',
			  type: 'post',
			  data: {'enroll_id': $(this).data('enroll_id')},
			  dataType: "json",
			  success: function(data, status) {
			  	  $('#ussenrollid').val(data.enroll_id);
				  $('#usssubjcode').val(data.subj_code);
				  $('#ussstudnum').val(data.stud_num);
				  $('#ussprofnum').val(data.prof_num);
				  $('#ussstatus').val(data.status);
				  $('#ussname').val(data.description);
				  $('#ussunits').val(data.units);
				  $('#ussroom').val(data.room);
				  $('#usssched').val(data.sched);

			  
			  },
			  error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			  }
      });
	
	});
	
});

$(function grade() {
		
		$("a.edit-grade").on("click", function(event){
			event.preventDefault();
            $('#editGrade').modal('show');


		$.ajax({
		url:'profprofile/retGrade',
			  type: 'post',
			  data: {'enroll_id': $(this).data('enroll_id')},
			  dataType: "json",
			  success: function(data, status) {
			  	  $('#ussenrollid').val(data.enroll_id);
				  $('#ugrade').val(data.grade);

			  
			  },
			  error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			  }
      });
	
	});
	
});

 function showAlert(){
   $("#myAlert").addClass("in")
  }

   window.setTimeout(function() {
            $(".alert-message").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove(); 
            });
        }, 3000);







