<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url();?>">e-Blackboard</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      </ul>
       <form class="navbar-form navbar-right">
        <a href="logout"type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-user"></span> Logout</a>
      </form>
             
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

 <div class="page-header">
      <div class="large-12 columns"style="margin-top:75px;" align="center">
        <h1>Admin</h1>
      </div>
    </div>

<ul class="nav nav-tabs" role="tablist">
  <li class="active"><a href="#students" role="tab" data-toggle="tab">Students</a></li>
  <li><a href="#profs" role="tab" data-toggle="tab">Professors</a></li>
  <li><a href="#subjects" role="tab" data-toggle="tab">Subjects</a></li>
  <li><a href="#cman" role="tab" data-toggle="tab">Course Management</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane active" id="students">
    </br>
    <div class="col-lg-12">  
        <h3 align="center">Students List</h3>
        </br> 
        <div class="form-inline" align="right">
          <button data-toggle="modal" data-target="#newStudent" class="btn btn-primary">Add New Student</button>
        </div>
        </br>
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th width="100" style="text-align:center;">Student Number</th>
                  <th width="100"style="text-align:center;">First Name</th>
                  <th width="100"style="text-align:center;">Last Name</th>
                  <th width="100"style="text-align:center;">Birthdate</th>
                  <th width="100"style="text-align:center;">Address</th>
                  <th width="100" style="text-align:center;">Contact Number</th>
                  <th width="100" style="text-align:center;">Action</th>
                </tr>
              </thead>
              <tbody>
               <tr>
              <?php if(isset($query1)) : foreach($query1 as $row):?>
               <tr>
                 <td width="100" style="text-align:center;"><?php echo $row->stud_num;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->fname;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->lname;?></td>  
                 <td width="100" style="text-align:center;"><?php echo $row->bdate;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->address;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->cnum;?></td>
                 <td width="100" style="text-align:center;"><a href="<?php base_url('superadmin');?>" class="btn btn-primary edit-students" data-stud_num ="<?php echo $row->stud_num;?>">Edit</a></td>
                 </tr><?php endforeach;?>
                  <?php else: ?>
                  <th><div>No records</div></th>
                  <?php endif;?>
                </tr>
              </tbody>
            </table>
          </div>  
    </div>
  </div>
<div>
</div>

<div class="tab-pane tab-pane fade" id="profs">
<div class="container">
<div class="col-lg-12">
</br>
  <h3 align="center">Professors List</h3>
</br>
        <div class="form-inline" align="right">
          <button data-toggle="modal" data-target="#newProf" class="btn btn-primary">Add New Professor</button>
        </div>
        </br>
    <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th width="100" style="text-align:center;">Professor Number</th>
                  <th width="100"style="text-align:center;">First Name</th>
                  <th width="100"style="text-align:center;">Last Name</th>
                  <th width="100"style="text-align:center;">Birthdate</th>
                  <th width="100"style="text-align:center;">Address</th>
                  <th width="100" style="text-align:center;">Contact Number</th>
                  <th width="100" style="text-align:center;">Action</th>

                </tr>
              </thead>
              <tbody>
               <tr>
              <?php if(isset($query2)) : foreach($query2 as $row):?>
               <tr>
                 <td width="100" style="text-align:center;"><?php echo $row->prof_num;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->fname;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->lname;?></td>  
                 <td width="100" style="text-align:center;"><?php echo $row->bdate;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->address;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->cnum;?></td>
                  <td width="100" style="text-align:center;"><a href="<?php base_url('superadmin');?>" class="btn btn-primary edit-profs" data-prof_num="<?php echo $row->prof_num;?>">Edit</a></td>
                 </tr><?php endforeach;?>
                  <?php else: ?>
                  <th><div>No records</div></th>
                  <?php endif;?>
                </tr>
              </tbody>
            </table>
          </div>
 
    </div>
  </div>
  </div>

<div class="tab-pane tab-pane fade" id="subjects">
<div class="container">
<div class="col-lg-12">
</br>
  <h3 align="center">Courses List</h3>
</br>

<div class="form-inline" align="right">
          <button data-toggle="modal" data-target="#newSubj" class="btn btn-primary">Add New Subject</button>
        </div>
        </br>
    <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th width="100" style="text-align:center;">Subject Name</th>
                  <th width="100"style="text-align:center;">Description</th>
                  <th width="100"style="text-align:center;">Units</th>
                  <th width="100"style="text-align:center;">Room</th>
                  <th width="100"style="text-align:center;">Schedule</th>
                  <th width="100"style="text-align:center;">Action</th>
                </tr>
              </thead>
              <tbody>
               <tr>
              <?php if(isset($query3)) : foreach($query3 as $row):?>
               <tr>
                 <td width="100" style="text-align:center;"><?php echo $row->subj_code;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->description;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->units;?></td>  
                 <td width="100" style="text-align:center;"><?php echo $row->room;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->sched;?></td>
                 <td width="100" style="text-align:center;"><a href="<?php base_url('superadmin');?>" class="btn btn-primary edit-subj" data-subj_code="<?php echo $row->subj_code;?>" >Edit</a></td>
                 </tr><?php endforeach;?>
                  <?php else: ?>
                  <th><div>No records</div></th>
                  <?php endif;?>
                </tr>
              </tbody>
            </table>
          </div>
 
    </div>
  </div>
  </div>

  <div class="tab-pane tab-pane fade" id="cman">
<div class="container">
<div class="col-lg-12">
</br>
  <h3 align="center">Courses List</h3>
</br>

<div class="form-inline" align="right">
          <button data-toggle="modal" data-target="#newStudsubj" class="btn btn-primary">Add New Subject to Student</button>
        </div>
        </br>
    <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th width="100" style="text-align:center;">Enroll ID</th>
                  <th width="100" style="text-align:center;">Student Number</th>
                  <th width="100" style="text-align:center;">Professor Number</th>
                  <th width="100" style="text-align:center;">Subject Name</th>
                  <th width="100"style="text-align:center;">Description</th>
                  <th width="100"style="text-align:center;">Units</th>
                  <th width="100"style="text-align:center;">Room</th>
                  <th width="100"style="text-align:center;">Schedule</th>
                  <th width="100"style="text-align:center;">Action</th>
                </tr>
              </thead>
              <tbody>
               <tr>
              <?php if(isset($query4)) : foreach($query4 as $row):?>
               <tr>
                 <td width="100" style="text-align:center;"><?php echo $row->enroll_id;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->stud_num;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->prof_num;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->subj_code;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->description;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->units;?></td>  
                 <td width="100" style="text-align:center;"><?php echo $row->room;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->sched;?></td>
                 <td width="100" style="text-align:center;"><a href="<?php base_url('superadmin');?>" class="btn btn-primary edit-studsubj" data-enroll_id="<?php echo $row->enroll_id;?>" >Edit</a></td>
                 </tr><?php endforeach;?>
                  <?php else: ?>
                  <th><div>No records</div></th>
                  <?php endif;?>
                </tr>
              </tbody>
            </table>
          </div>
 
    </div>
  </div>
  </div>

<div class="modal fade" id="newStudent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">New Student</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open('superadmin/addStudent');?>

                    <div class="form-group">
                    <label>Student Number</label>
                    <input type="text" name="stud_num" class="form-control" id="studnum"/>
                    </div>

                    <div class="form-group">
                    <label>Password</label>
                    <input type="text" name="spassword" class="form-control" id="password"/>
                    </div>
        
                    <div class="form-group">
                    <label>First Name</label>
                    <input type="text" name="sfname" class="form-control" id="sfname"/>
                    </div>

                    <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" name="slname" class="form-control" id="slname"/>
                    </div>
                
                    <div class="form-group">
                     <label>Birthdate</label>
                     <input name="sbdate" id="sbdate" class="datepicker form-control" data-date-format="yyyy-mm-dd" type="text" />
                    </div>
                
                   <div class="form-group">
                      <label>Address</label>
                      <input type="text" name="saddress" class="form-control" id="saddress" />
                  </div>

                   <div class="form-group">
                      <label>Contact Number</label>
                      <input type="text" name="scnum" class="form-control" id="scnum" />
                  </div>
                 
                    
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <input type="submit" name="Submit" value="Create"  class="btn btn-success">
        <?php echo form_close();?>  
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editStudent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Student</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open('superadmin/updateStudent');?>

                
                    <input name="stud_num" class="hidden" id="ustudnum"/>

                    <div class="form-group">
                    <label>First Name</label>
                    <input type="text" name="sfname" class="form-control" id="usfname"/>
                    </div>

                    <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" name="slname" class="form-control" id="uslname"/>
                    </div>
                
                    <div class="form-group">
                     <label>Birthdate</label>
                     <input name="sbdate" id="usbdate" class="datepicker form-control" data-date-format="yyyy-mm-dd" type="text" />
                    </div>
                
                   <div class="form-group">
                      <label>Address</label>
                      <input type="text" name="saddress" class="form-control" id="usaddress" />
                  </div>

                   <div class="form-group">
                      <label>Contact Number</label>
                      <input type="text" name="scnum" class="form-control" id="uscnum" />
                  </div>

                  <div class="form-group">
                    <label>Password</label>
                    <input type="text" name="spassword" class="form-control" id="uspassword"/>
                  </div>
                 
                    
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <input type="submit" name="Submit" value="Update"  class="btn btn-success">
        <?php echo form_close();?>  
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="newProf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">New Professor</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open('superadmin/addProf');?>

                    <div class="form-group">
                    <label>Professor Number</label>
                    <input type="text" name="profnum" class="form-control" id="profnum"/>
                    </div>

                    <div class="form-group">
                    <label>Password</label>
                    <input type="text" name="ppassword" class="form-control" id="password"/>
                    </div>
        
                    <div class="form-group">
                    <label>First Name</label>
                    <input type="text" name="pfname" class="form-control" id="pfname"/>
                    </div>

                    <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" name="plname" class="form-control" id="plname"/>
                    </div>
                
                    <div class="form-group">
                     <label>Birthdate</label>
                     <input name="pbdate" id="pbdate" class="datepicker form-control" data-date-format="yyyy-mm-dd" type="text" />
                    </div>
                
                   <div class="form-group">
                      <label>Address</label>
                      <input type="text" name="paddress" class="form-control" id="paddress" />
                  </div>

                   <div class="form-group">
                      <label>Contact Number</label>
                      <input type="text" name="pcnum" class="form-control" id="pcnum" />
                  </div>
                 
                    
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <input type="submit" name="Submit" value="Create"  class="btn btn-success">
        <?php echo form_close();?>  
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editProf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Professor</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open('superadmin/updateProf');?>

                
                    <input name="prof_num" class="hidden" id="uprofnum"/>
                  
        
                    <div class="form-group">
                    <label>First Name</label>
                    <input type="text" name="pfname" class="form-control" id="upfname"/>
                    </div>

                    <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" name="plname" class="form-control" id="uplname"/>
                    </div>
                
                    <div class="form-group">
                     <label>Birthdate</label>
                     <input name="pbdate" id="upbdate" class="datepicker form-control" data-date-format="yyyy-mm-dd" type="text" />
                    </div>
                
                   <div class="form-group">
                      <label>Address</label>
                      <input type="text" name="paddress" class="form-control" id="upaddress" />
                  </div>

                   <div class="form-group">
                      <label>Contact Number</label>
                      <input type="text" name="pcnum" class="form-control" id="upcnum" />
                  </div>

                  <div class="form-group">
                    <label>Password</label>
                    <input type="text" name="ppassword" class="form-control" id="uppassword"/>
                  </div>
                 
                    
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <input type="submit" name="Submit" value="Update"  class="btn btn-success">
        <?php echo form_close();?>  
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="newSubj" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">New Subject</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open('superadmin/addSubj');?>

                    <div class="form-group">
                    <label>Subject Name</label>
                    <input type="text" name="subjcode" class="form-control" id="subjcode"/>
                    </div>
        
                    <div class="form-group">
                    <label>Description</label>
                    <input type="text" name="name" class="form-control" id="name"/>
                    </div>

                    <div class="form-group">
                    <label>Units</label>
                    <input type="text" name="units" class="form-control" id="units"/>
                    </div>
                
                     <div class="form-group">
                    <label>Professor Number</label>
                    <input type="text" name="cprofnum" class="form-control" id="cprofnum"/>
                    </div>

                   <div class="form-group">
                      <label>Room</label>
                      <input type="text" name="room" class="form-control" id="room" />
                  </div>

                   <div class="form-group">
                      <label>Schedule</label>
                      <input type="text" name="sched" class="form-control" id="sched" />
                  </div>
                 
                    
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <input type="submit" name="Submit" value="Create"  class="btn btn-success">
        <?php echo form_close();?>  
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editSubj" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Subject</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open('superadmin/updateSubj');?>

                    
                    <input type="text" name="subj_code" class="hidden" id="usubjcode"/>
                  
        
                    <div class="form-group">
                    <label>Description</label>
                    <input type="text" name="name" class="form-control" id="uname"/>
                    </div>

                    <div class="form-group">
                    <label>Units</label>
                    <input type="text" name="units" class="form-control" id="uunits"/>
                    </div>
                
                     <div class="form-group">
                    <label>Professor Number</label>
                    <input type="text" name="cprofnum" class="form-control" id="ucprofnum"/>
                    </div>

                   <div class="form-group">
                      <label>Room</label>
                      <input type="text" name="room" class="form-control" id="uroom" />
                  </div>

                   <div class="form-group">
                      <label>Schedule</label>
                      <input type="text" name="sched" class="form-control" id="usched" />
                  </div>
                 
                    
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <input type="submit" name="Submit" value="Update"  class="btn btn-success">
        <?php echo form_close();?>  
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="newStudsubj" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Enroll New Student</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open('superadmin/addStudsubj');?>

                    <div class="form-group">
                    <label>Student Number</label>
                    <input type="text" name="ssstudnum" class="form-control" id="studnum"/>
                    </div>

                    <div class="form-group">
                    <label>Subject Name</label>
                    <input type="text" name="sssubjcode" class="form-control" id="subjcode"/>
                    </div>
                 
                    
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <input type="submit" name="Submit" value="Create"  class="btn btn-success">
        <?php echo form_close();?>  
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editStudsubj" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Enrolled Students</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open('superadmin/updateStudsub');?>

                    
                    <input type="text" name="enroll_id" class="hidden" id="ussenrollid"/>

                    <div class="form-group">
                    <label>Student Number</label>
                    <input type="text" name="ssstudnum" class="form-control" id="ussstudnum"/>
                    </div>
                    
                    <div class="form-group">
                    <label>Subject Name</label>
                    <input type="text" name="sssubjcode" class="form-control" id="usssubjcode"/>
                    </div>

                  <div class="form-group">
                      <label>Status</label>
                      <input type="text" name="ssstatus" class="form-control" id="ussstatus" />
                  </div>
                 
                    
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <input type="submit" name="Submit" value="Update"  class="btn btn-success">
        <?php echo form_close();?>  
      </div>
    </div>
  </div>
</div>