<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url();?>">e-Blackboard</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="#welcome"><span class="glyphicon glyphicon-home"></span> Home</a></li>
        <li><a href="#contact"><span class="glyphicon glyphicon-th-large"></span> Courses</a></li>
         <li><a href="#contact"><span class="glyphicon glyphicon-info-sign"></span> About Us</a></li>
          <li><a href="#contact"><span class="glyphicon glyphicon-phone-alt"></span> Contact Us</a></li>
      </ul>
     
             
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

 <div class="page-header">
    <div class="large-12 columns"style="margin-top:75px;" align="center">
      <h1><span class="label label-info">Search Results</span></h1>
    </div>
  </div>

<form class="col-lg-12 form-inline" action="<?php echo site_url('search/search_keyword');?>" method = "post" align="right">
<div class="form-group">
<input type="text" placeholder="Search" name="keyword" class="form-control">
<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Submit</button>
</div>
</form>

 <div class="row">
            <div class="col-xs-offset-3 col-md-offset-3">  
              <h2></h2>                   
            </div>
          </div>
           <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th width="100" style="text-align:center;">First Name</th>
                  <th width="100"style="text-align:center;">Last Name</th>
                  <th width="100"style="text-align:center;">Subject Name</th>
                  <th width="100"style="text-align:center;">Units</th>
                  <th width="100"style="text-align:center;">Room</th>
                  <th width="100"style="text-align:center;">Schedule</th>
                </tr>
              </thead>
              <tbody>
               <tr>
              <?php if(isset($results)) : foreach($results as $row):?>
               <tr>
                 <td width="100" style="text-align:center;"><?php echo $row->fname;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->lname;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->subj_code;?></td>  
                 <td width="100" style="text-align:center;"><?php echo $row->units;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->room;?></td>
                 <td width="100" style="text-align:center;"><?php echo $row->sched;?></td>
                 </tr><?php endforeach;?>
                  <?php else: ?>
                  <th><div>No records</div></th>
                  <?php endif;?>
                </tr>
              </tbody>
            </table>
          </div>         