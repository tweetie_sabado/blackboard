

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>css/signin.css" rel="stylesheet">

 

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

      <?php $att = array('class' => 'form-signin', 'role' => 'form'); echo form_open('login/validate_account', $att);?>
        <h2 class="form-signin-heading">Please sign in</h2>
        <input name="myusername" id="myusername" class="form-control" placeholder="Username" required autofocus>
        <input name="mypassword" id="mypassword" type="password" class="form-control" placeholder="Password" required>
        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <?php echo form_close();?>

    </div> <!-- /container -->


  </body>
