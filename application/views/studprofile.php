<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url();?>">e-Blackboard</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="#welcome"><span class="glyphicon glyphicon-home"></span> Home</a></li>
        <li><a href="#contact"><span class="glyphicon glyphicon-th-large"></span> Courses</a></li>
         <li><a href="#contact"><span class="glyphicon glyphicon-info-sign"></span> About Us</a></li>
          <li><a href="#contact"><span class="glyphicon glyphicon-phone-alt"></span> Contact Us</a></li>
      </ul>
       <form class="navbar-form navbar-right">
        <a href="<?php echo base_url();?>studlogin/logout"type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-user"></span> Logout</a>
      </form>
             
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

 <div class="page-header">
    <div class="large-12 columns"style="margin-top:75px;" align="center">
      <h1>Student Information</h1>
    </div>
  </div>



  <form class="col-md-offset-9 form-inline" action="<?php echo site_url('search/search_keyword');?>" method = "post" align="right">
    <div class="form-group">
      <input type="text" placeholder="Search" name="keyword" class="form-control">
      <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Submit</button>
    </div>
  </form>


<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">
      <ul class="nav nav-pills nav-stacked">
        <li class="active" role="presentation"><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> My Profile</a></li>
        <li role="presentation"><a href="#courses" data-toggle="tab"><span class="glyphicon glyphicon-book"></span> My Courses</a></li>
        <li role="presentation"><a href="#enrollment" data-toggle="tab"><span class="glyphicon glyphicon-pencil"></span> Online Enrollment</a></li>
        <li role="presentation"><a href="#grades" data-toggle="tab"><span class="glyphicon glyphicon-list-alt"></span> My Grades</a></li>
      </ul>
    </div>
    
    <div class="col-md-9 tab-content">

       <div class="tab-pane tab-pane fade in active" id="profile">
          <?php foreach($query as $row):?>   
                     
            <div class="row">
              <div class="col-xs-offset-3 col-md-offset-3">  
                <h2><span class="label label-info">My Profile</span></h2>                
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAbrwAAG68BXhqRHAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAA0cSURBVHic7Z17rFxFHcc/5a0tloegCLYzeMCCTQAFFXzQBxBKCcVS6iO0pWgMIFiwvoJNsAgBebWIoX8AlkKJpinVmkC10AtNYxvEaKsNFBl6BlOUV4HYXhEqt/4xs9zt7e2e58yc3T2fZLPJ3rPn97vn9905c2Z+85shO3fupKZ72Su0AzVhqQXQ5dQC6HJqAXQ5tQC6nFoAXc4+oR1wRSTk/sAxwCj7GgkcCAxreh9mD99uX9ua3l8ANtnXc0rHb/v03xdDOmEcIBJyCHACMA44HRgNCMpr4foADWwEVgM9wAal47a/eG0rgEjII4FJmKCPAQ717MJW4AmMGJYrHb/o2X4ptJUAIiGHApOBacB4qtOH6QNWAQ8Ay5SOewP7k5q2EEAk5KnAZZjgDw3sThK9wDJggdLxutDOJFFpAURCjgXmYJr5dqQHuF7p+PHQjuyJSgogEnICJvCnhfalJNZihLAitCMDqZQAIiGPB+7C9OQ7kdXA5UrHT4d2pEElBGA7d9cCVwH7BnbHNTuA+cDcKnQWgwsgEnIycAdwVFBH/LMFmKV0vCykE8EEEAn5fuBO4JIgDlSHhcAVSsf/CWE8iAAiIY8DlmBG7GrMCONUpeNnfBv2PpASCTkdeIo6+M2MBp6y18Yr3loAO15/G3C1F4Ptyzxgtq95Bi8CiITcB3Ovu8i5sc5gMTBT6fh/rg05F4Dt7C0FJjg11HmsAKa47hw6FUAk5MHAw8Cpzox0NuuAiUrHb7gy4EwA9pf/GHXwi7IOOMNVS+DkKcDe85dSB78MTgWW2mtaOqULwPb2F1Lf88tkArDQXttScdEC3Ebd23fBRZhrWyql9gHsQMai0k5YMxgzlI7vL+tkpQnADu8+RfUzdtqdXuCUsoaNS7kF2B7/Eurg+2AosMRe88KU1bP8OdUf218JLAeex/yK9sX0sBsv31nFRRiNueaFZ1IL3wLsfP5DRR1xyLOYeffftzooEvJYjBDGAOcDB7l3rTAXFM0nKCQAm8mziWomc2wDrgPuUDrekeWLkZD7AWcDM4Fzqe4Kqi3AqCKZRUUFcDPwvdwncMefgXOVjv9V9ESRkEdghDAbOKTo+Rxwi9Lx9/N+ObcAbALneqqXw/cn4Kyyx88jIQ8Dbqd6Yxw7gBPzJpoWeQq4i+oF/4/AmS4mT5SOX1U6ngZ8CwifSdvPvphY5CJXC2Dz9h/Ja9QRbwKR0vFW14YiIWdghrtLH5otwDl51h3kbQHm5PyeS270EXwApeNFQGmjcSWRKyaZBWCXa1Vtxc4W4GeebV6DGU+oCqfZ2GQiTwtQxV//tUrH//VpUOn4n8DNPm2mIHNsMgnArtKt2kLNtwk3AXUr8E4g24MxzsYoNVlbgMsyHu8DpXT8bhDDJkvHey5/AplilFoATcUZqsbfA9v/a2D7A5lsY5WKLC1AVYsz/Duw/Q2B7Q8k0w81iwCmZffFCx8MbP/VwPYHI3WsUgnAFmQan9sdt4QWwPDA9gdjvI1ZImlbgEkZjvVNLYDd2QsTs1QHpqFqj37NhBbAwYHt74lUMUsUgE1FHlPUG4cMj4QMOSl1fEDbrRiTJo08TQtwAtVPlwrp38kBbbfiUEzsWpJGAFVu/hsMSz6kfCIhRxD+FtSKxNilEUDVK3atVzpWgWxvD2Q3LYmxSyOAqmf73hPKsNLx68CaUPZTkBi7lgKwJddFWd44Yn1g+1UbCm5G2BjukaQW4JgUx4TmgMD2q5gR3WAvTAxbHtCKUeX54ozEnq5jqvoY2KBlDDtBAJ8MZTgS8gDgY6Hsp6SQAEaW6IgrJkVCfiCQ7Quo/i2yZQyTnD+wREdcMQy42LdRO8r2Q992c9AyhkkCCDLAkoNZkZB7e7Y5geo/IkNCDDuhBQA4Gvi8Z5sXe7aXl65oAcBjuppdmz/Rl72CFGoB2kkA53u0NREopUCDBwoJoJ0YEQn5KU+2LvRkxzlJAqj6ZMdAvuTaQJs1/5AQw04TwIUengam0z7NPxQUwLYSHfHBsZhCDk6IhBxJ9ZaDJdEyhp3WAgBcZ0vWlUpTBdR2eTRu0FUtAMD+wCIHt4IrgcyrbytAoRbghRId8ckpQO66OQOxFcRuKut8nmkZwyQBbCrREd/8OOtK2cGwRaKWAO8r7lIQWsawkwWwH9ATCfm1vCeIhPwMpuhU6JyDIhQSwHOYrdHblQOAByMhb8haaj0SciZmq9ePOPHMD32YGO6RxCJRkZDPYyZb2onXgM3Ap5s++w0wTem4Za/YbsxwG/Btd+55Y7PSccuElTRDwRtLcsYXr2MWsn4O+AnQKB5xPvBsJOQ1kZC7LSSJhPxwJOQcIGbX4P8O+Dhmb+MghSgKkBi7NC3Ad3CwUYEjNgDTlY7fy9S1HcHF7NqKvQtoTB3hYZjEySMGnOtl4Cql4181nesLmOpgwoHvLpitdHx7qwPStAA9JTnjkmeAqcBJzcEHUDpeB5wI3Nf08d6YXL5zgC+ya/B3YtYaHNccfHuuNcAngJ8CXotS5SQxdmlagCGYIghVXB/4PDAXeFDpOLGzGgl5EqbU6wXACEyhxz7gaeBJ+1qjdJz49BMJeRSmaugMdm89qsBW4LCkHUhTVQqNhFyKuWhVQQM3APfl3V3TZvQeBbyU1DFMOM8+mNnBb2IqjFdliv0hpeMpSQelLYPeQ3gBvIbZiu6XmF9poXq9tq5g4TWFVoDLgeWRkB8Fvm5foReMpLp1p20BjgT+gX919wLLMEF/1MdeumVg5yHOxuzocS5mUMonfcAIpeMXkw5MXSw6EnIlcGZBx9LShyn++KMyav6HJBJSAjcCX/Zo9lGl47PSHJjlF/1ATmey8iRwstLxJe0efACl41jp+CvAZzHl7H2QOlZZBLAM98WRVwJjlY7/4tiOd5SOn8Ss1/+tY1ON22YqUgvA7ktTaIOiBLYDX1U6fsuhjaDYjucM3OZZLMuyh1DWTt2CjMdnOrctuNDRKB2/SYEdPlKQKUaZBGBH1VyMDL5F+ww3l8HtmP+5bHpsjFKT57Hu+hzfSeIepeOXHZy3kigdvwLc7eDUmWOTWQBKx48Da7N+rwU7aL9M2zK4BShzXGOtjU0m8g7slNkKrFE63lLi+doC+z//ocRT5opJLgHY3alW5/nuIFRt9zGfZN7law+szrNjGBQb2r0c03wXpRZAMXZgYpGL3AKwO1XOz/t9i1Y6rtqWK96wuQuJ4/UJzM+7aygUn9yZi9myLS9lNYHtTJFrsAUTg9wUEoAdcZpV4BQri9jvEB4t8N1ZRXYOhxKmd+3+9Qtzfr3dEk5dkPcaLLTXvhBlze9fQfZ/ZAcms6fbUWTPNt6IueaFKUUAdv+8qWSbLdzcLgkeLlE6fgeTip6WXmCqveaFKS3Dx/bmszyOhN7vr0o8m+HYy8t8cio1xUvp+H5gXsrDWy5Z6jLSCmCevcal4SLHbzZmIUYSdQvQT5pFuItxUP2kdAHYbN2ZJD/f1gLoJ6kFWAHMLJoJPRhOsnxt524K0GpuuhZAP60EsA6Y4qrDnDorOA+RkAcDDwMDCzX0Kh23UxFK50RCvsnum1CuAyYqHb/hyq7TPH/r+BnsfjuoO4C7M7AVWAGc4TL44GGhh31ePY9dO4bDIyFbbmXSTURCHg0c1PTRYuC8sp71W+FlpY+9f02n/xFRAusjIa/MWrmj04iEvBSzrP1Y+9E8zBJ3L4NkTvsAgxEJOR2TFTvUftQDXKJ03K4VyXJh1xHeS/9qq17MIE+pz/lJeBcAgC3kuIT+DRe2AVcrHd/r3ZkA2PpD84HGVjcbMcO73nMjgggA3iu6fCdmAWWDR4BvdMKSsMGwJefuZtdi0wuBK3zc7wcjmAAaREJOBu6gfzn1G8APgEV2oqTtiYTcD7Mi6CbgEPvxFsx8vsvVVokEFwBAJORQTBGmq4DGVvAvYVqIBa4fhVxhx0EuxZSZbVQR2YFp/ucWTeYog0oIoEEk5PGYDmLzpse9wC8wuW+bgziWkUhIAVyNub01D3itxnT0cufwlU2lBNAgEnICMAc4renjd4FfA7falbaVIxLyZOC7mGHw5mLVa4Hr86Zuu6SSAmgQCTkWI4RxA/60FlMuZhXwNxeTJGmwYxijMf5NxlQca6YHE/jMK3Z8UWkBNLC1/i7DXOShA/78KuZCrwJWub5N2Iof4+1rHHD4gEMay+gXZF2oGYK2EEAD21mcDEzDBGCwkUwNPAY8jikj9zLwStbHLPuYejjwIUxNwbHWphzk8D6MAB8g4/r80LSVAJqxhasmYX6FY0iuY7gdeAUriKZ36A9083vSbOVW4AlM67M8TUGmKtK2AmjG3otPwIjhdMx9WVDeXEcfpmXZiOnJ9wAbQvU9yqQjBDAYkZD7Y2oAj7KvkZj9foY1vTd+5dvta1vT+wuYVK1NwHNKx2/79N8XHSuAmnRUpaxpTSBqAXQ5tQC6nFoAXU4tgC6nFkCX83/xiiER/m1ruQAAAABJRU5ErkJgggb0c36fb324a9eaecc8699f0cdca9ba2f" class="img-circle"/>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="form-group col-md-3">
                <label for="disabledTextInput">Student Number</label>  
                <input id="disabledTextInput" class="form-control" type="text" placeholder=" <?php echo $row->stud_num;?>" disabled/>
              </div>
              <div class="form-group col-md-3">
                <label for="disabledTextInput">First Name</label>  
                <input id="disabledTextInput" class="form-control" type="text" placeholder=" <?php echo $row->fname;?>" disabled/>
              </div>
              <div class="form-group col-md-3">
                <label for="disabledTextInput">Last Name</label>  
                 <input id="disabledTextInput" class="form-control" type="text" placeholder=" <?php echo $row->lname;?>" disabled/>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-3">
                <label for="disabledTextInput">Birthdate</label>  
                <input id="disabledTextInput" class="form-control" type="text" placeholder=" <?php echo $row->bdate;?>" disabled/>
              </div>
              <div class="form-group col-md-3">
                <label for="disabledTextInput">Address</label>  
                <input id="disabledTextInput" class="form-control" type="text" placeholder=" <?php echo $row->address;?>" disabled/>
              </div>
              <div class="form-group col-md-3">
                <label for="disabledTextInput">Contact Number</label>  
                <input id="disabledTextInput" class="form-control" type="text" placeholder=" <?php echo $row->cnum;?>" disabled/>
              </div>
            </div>
          <?php endforeach;?>
       </div>


       <div class="tab-pane tab-pane fade" id="courses">
          
          <div class="row">
            <div class="col-xs-offset-3 col-md-offset-3">  
              <h2><span class="label label-info">My Courses</span></h2>                   
            </div>
          </div>
           <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th width="50" style="text-align:center;">Subject Name</th>
                  <th width="100"style="text-align:center;">Description</th>
                  <th width="100"style="text-align:center;">Units</th>
                  <th width="100"style="text-align:center;">Room</th>
                  <th width="100"style="text-align:center;">Schedule</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <?php if(isset($records)) : foreach($records as $row):?>
                  <tr>
                  <td width="10" style="text-align:center;"><?php echo $row->subj_code;?></td>
                  <td width="100" style="text-align:center;"><?php echo $row->description;?></td>
                  <td width="100" style="text-align:center;"><?php echo $row->units;?></td>  
                  <td width="100" style="text-align:center;"><?php echo $row->room;?></td>
                  <td width="100" style="text-align:center;"><?php echo $row->sched;?></td>
                  </tr><?php endforeach;?>
                </tr>
                <tr>
                  <?php else: ?>
                   <span class="label label-danger">No records found!</span>
                  <?php endif;?>
                </tr>
              </tbody>
            </table>
          </div>
       </div>

       <div class="tab-pane tab-pane fade" id="enrollment">
          <div class="row">
            <div class="col-xs-offset-1 col-md-offset-3"> 
              <h2><span class="label label-info">Online Enrollment</span></h2>        
            </div>
          </div>
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th width="50" style="text-align:center;">Enrollment ID</th>
                  <th width="50" style="text-align:center;">Subject Name</th>
                  <th width="100"style="text-align:center;">Description</th>
                  <th width="100"style="text-align:center;">Units</th>
                  <th width="100"style="text-align:center;">Room</th>
                  <th width="100"style="text-align:center;">Schedule</th>
                  <th width="100" style="text-align:center;">Action</th>
                </tr>
              </thead>
              <tbody>
                 <tr>
                  <?php if(isset($records_not)) : foreach($records_not as $row):?>
                  <tr>
                  <td width="10" style="text-align:center;"><?php echo $row->enroll_id;?></td>
                  <td width="10" style="text-align:center;"><?php echo $row->subj_code;?></td>
                  <td width="100" style="text-align:center;"><?php echo $row->description;?></td>
                  <td width="100" style="text-align:center;"><?php echo $row->units;?></td>  
                  <td width="100" style="text-align:center;"><?php echo $row->room;?></td>
                  <td width="100" style="text-align:center;"><?php echo $row->sched;?></td>
                  <td align="center"><a href="<?php echo base_url().'studprofile/enroll/'.$row->enroll_id?>" class="btn btn-primary">Enroll</a></td>
                  </tr><?php endforeach;?>
                 </tr>
                 <tr>
                  <?php else: ?>
                  <span class="label label-danger">No records found!</span>
                  <?php endif;?>
                 </tr>
              </tbody>
            </table>
          </div>
        </div>


       <div class="tab-pane tab-pane fade" id="grades">
          <div class="row">
            <div class="col-xs-offset-3 col-md-offset-3"> 
              <h2><span class="label label-info">My Grades</span></h2>        
            </div>
          </div>
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th width="50" style="text-align:center;">Subject Name</th>
                  <th width="100"style="text-align:center;">Description</th>
                  <th width="100"style="text-align:center;">Units</th>
                  <th width="100"style="text-align:center;">Grade</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                <?php if(isset($query2)) : foreach($query2 as $row):?>
                <tr>
                  <td width="10" style="text-align:center;"><?php echo $row->subj_code;?></td>
                  <td width="100" style="text-align:center;"><?php echo $row->description;?></td>
                  <td width="100" style="text-align:center;"><?php echo $row->units;?></td>
                  <td width="100" style="text-align:center;"><?php echo $row->grade;?></td>
                </tr><?php endforeach;?>

                <?php else: ?>
                <span class="label label-danger">No records found!</span>
                <?php endif;?>
                </tr>
              </tbody>
            </table>
          </div>
       </div>

    </div>

  </div>
</div>
