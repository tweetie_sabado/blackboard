<?= doctype("html5")?>
 
<html>
    <head>
        <title><?= $title ?></title>
        <link rel="stylesheet" href="<?php echo base_url('css/foundation.cal.css'); ?>" />
        <script src="<?php echo base_url('js/vendor/modernizr.js');?>"></script>
<?php
   
    $link = array(
              'href' => base_url().'application/css/default.css',
              'rel' => 'stylesheet',
              'type' => 'text/css'
    );
    
    
    echo link_tag($link);
    echo $library_src;
    echo $script_foot;
?>
</head>
<body>
		<section class="top-bar-section">
		<ul class="right button-group [radius round]"> 
			<li class="has-form"> 
			<a href="<?php echo base_url('postlogin'); ?>" class="button alert radius">Back to Home</a> 
			</li>
			</ul>
    <div id="wrapper">
        <div id="header">
            <h1><?= $title ?> </h1>
            
            
