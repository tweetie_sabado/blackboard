<!doctype html>
<html lang="en">
	<head>
		<meta charset = "UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Annyeong Tomasino</title>
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link href="jumbotron.css" rel="stylesheet">
		<link href="carousel.css" rel="stylesheet">
	</head>

	<body>
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><font color="#ffffff">Annyeong Tomasino</font></a>
				</div>
				
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">Home</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">ADMIN<b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="#">Member List</a></li>
									<li><a href="#">Create Event</a></li>
									<li><a href="#">Create Post</a></li>
								</ul>
						</li>
						
						<li> <a href="#">AT Website</a></li>
						
						<li><a href="http://osa.ust.edu.ph/" target="_blank">Office for Student Affairs</a></li>
					</ul>
					
					
				</div><!--/.navbar-collapse -->
			</div>
		</div>

		<!-- Main jumbotron for a primary marketing message or call to action -->
		<div class="jumbotron">
			<div class="container text-center">
				<h2>REGISTER</h2>
				<p> Sign up for the university's organization for the Korean culture and arts. Click the button below.</p>
				<a href="#myModal" role="button" class="btn btn-warning" data-toggle="modal">Register!</a>
				

<!--- start of modal -->
<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal">&times;</button>
				
				<h4 class="modal-title">Registration</h4>
			</div><!-- end modal-header -->
			<div class="modal-body">
				<h4>Please fill up the following fields.</h4>
				
				<hr>
				
				
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-lg-2 control-label" for="inputLName">Last Name</label>
						<div class="col-lg-10">
							<input class="form-control" id="inputLName" placeholder="Last Name" type="text">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 control-label" for="inputFName">First Name</label>
						<div class="col-lg-10">
							<input class="form-control" id="inputFName" placeholder="First Name" type="text">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 control-label" for="inputMessage">E-mail</label>
						<div class="col-lg-10">
							<input class="form-control" id="inputEmail" placeholder="Email" type="text">
						</div>
					</div>
					
				
					<div class="form-group">
						<label class="col-lg-2 control-label" for="inputBday">Birthday</label>
						<div class="col-lg-10">
							<input class="form-control" id="inputBday" placeholder="MM/DD/YYYY" type="text">
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-2 control-label" for="inputSNo">Student Number</label>
						<div class="col-lg-10">
							<input class="form-control" id="inputSNo" placeholder="Student Number" type="text">
						</div>
					</div>	
					
					<div class="form-group">
						<label class="col-lg-2 control-label" for="inputCollege">College</label>
						<div class="col-lg-10">
						<div class="btn-group">
							<button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"> Please select. <span class="caret"></span></button>
							<ul class="dropdown-menu" role="menu">
									<li><a href="#">AB</li></a>
									<li><a href="#">Accountancy</li></a>
									<li><a href="#">Architecture</li></a>
									<li><a href="#">CFAD</li></a>
									<li><a href="#">Civil Law</li></a>
									<li><a href="#">Commerce</li></a>
									<li><a href="#">CRS</li></a>
									<li><a href="#">CTHM</li></a>
									<li><a href="#">Education</li></a>
									<li><a href="#">Engineering</li></a>
									<li><a href="#">IICS</li></a>
									<li><a href="#">IPEA</li></a>
									<li><a href="#">Medicine & Surgery</li></a>
									<li><a href="#">Music</li></a>
									<li><a href="#">Nursing</li></a>
									<li><a href="#">Pharmacy</li></a>
									<li><a href="#">Science</li></a>
							</ul>
						</div>
						</div>
					</div>
							
							
							<button class="btn btn-success pull-right" type="submit">Register!</button>
						</div>
					</div>
				</form>
				<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" type="button">Close</button> <button class="btn btn-primary" type="button">Save changes</button>
			</div><!-- end modal-footer -->
			</div><!-- end modal-body -->
			
			
		</div><!-- end modal-content -->
	</div><!-- end modal-dialog -->
</div><!-- end myModal -->			

			</div>
		</div>
		
			<!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

		<div class="container marketing">
			<!-- Three columns of text below the carousel -->
			<div class="row text-center">
				<div class="col-lg-4">
					<img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" style="width: 140px; height: 140px;">
					<h2>Member List</h2>
					<p>Check the list of members and the events they participated. Click the button below or check the ADMIN tab above.</p>
					<p><a class="btn btn-default" href="#" role="button">Member List</a></p>
				</div><!-- /.col-lg-4 -->
				
				<div class="col-lg-4">
					<img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" style="width: 140px; height: 140px;">
					<h2>Create Event</h2>
					<p>Create an event and let the members RSVP. Click the button below or check the ADMIN tab above.</p>
					<p><a class="btn btn-default" href="#" role="button">Start an Event</a></p>
				</div><!-- /.col-lg-4 -->
				
				<div class="col-lg-4">
					<img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" style="width: 140px; height: 140px;">
					<h2>Create Post</h2>
					<p>Update the website by posting news and announcements. Click the button below or check the ADMIN tab above. </p>
					<p><a class="btn btn-default" href="#" role="button">Make a Post</a></p>
				</div><!-- /.col-lg-4 -->
				
				
		</div> <!-- /container -->
		
		


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="../../dist/js/bootstrap.min.js"></script>
		<script src="../../assets/js/docs.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		
		
	</body>
</html>
