<?php

class Profprofile_model extends CI_Model{

	function check_account_db(){
        $this->db->where('prof_num', $this->input->post('myusername'));
        $this->db->where('prof_pass',$this->input->post('mypassword'));
        
        $query = $this->db->get('profinfo');
		if($query->num_rows == 1)
		{
                $row = $query->row();
                $data = array(
                'prof_id' => $row->stud_id,
                'prof_num' => $row->stud_num,
                'is_logged_in' => true
                );
                $this->session->set_userdata($data);
                return true;
		}
		else{
                return false;	
		}	   
    }

	function get_profile($prof_num){
		$this->db->where('prof_num',$prof_num);
		$query = $this->db->get('profinfo');
		if($query->num_rows() >0){
			foreach($query->result() as $row){
			$data[]=$row;	
			}
		}
		return $data;
	}

	function get_courses($prof_num){
		$this->db->where('prof_num',$prof_num);
		$query = $this->db->get('subjects');
		if($query->num_rows() >0){
			foreach($query->result() as $row){
			$data[]=$row;	
			}
		}
		return $data;
	}


	function get_students($prof_num){
		$this->db->where('ss.prof_num',$prof_num);
		$this->db->where('status','1');
		$this->db->join('studinfo s', 's.stud_num = e.stud_num', 'right');
		$this->db->join('subjects ss', 'ss.subj_code = e.subj_code', 'right');
		$query=$this->db->get('stud_subjects e'); 
        if($query->num_rows() >0){
			foreach($query->result() as $row){
			$data[]=$row;	
			}
		}
		return $data;
	}

	function update_grade($enroll_id, $data)
	
	{
		$this->db->where('enroll_id', $enroll_id);
		$this->db->update('stud_subjects', $data);
		
	}


	function get_grade($enroll_id)
	
	{
		$this->db->where('enroll_id',$enroll_id);
		$query = $this->db->get('stud_subjects');
		return $query->row();
	
	}


}