<?php

Class Search_model Extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function search($keyword)
    {
        $this->db->like('fname', $keyword); // users table
		$this->db->or_like('lname', $keyword); // companies table
		$this->db->or_like('subj_code', $keyword); // companies table
		$this->db->or_like('room', $keyword); // interests table        
		$this->db->from('subjects s');
		$this->db->join('profinfo p', 'p.prof_num = s.prof_num', 'left');
		$this->db->group_by('s.subj_code'); // added a group_by
		$query=$this->db->get(); 
        return $query->result();
    }
}   