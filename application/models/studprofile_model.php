<?php

class Studprofile_model extends CI_Model{

	function check_account_db(){
        $this->db->where('stud_num', $this->input->post('myusername'));
        $this->db->where('password',$this->input->post('mypassword'));
        
        $query = $this->db->get('studinfo');
		if($query->num_rows == 1)
		{
                $row = $query->row();
                $data = array(
                'stud_id' => $row->stud_id,
                'stud_num' => $row->stud_num,
                'is_logged_in' => true
                );
                $this->session->set_userdata($data);
                return true;
		}
		else{
                return false;	
		}	   
    }

	function get_profile($stud_num){
		$this->db->where('stud_num',$stud_num);
		$query = $this->db->get('studinfo');
		if($query->num_rows() >0){
			foreach($query->result() as $row){
			$data[]=$row;	
			}
		}
		return $data;
	}

	function get_enrolled_courses($stud_num){
		$this->db->where('stud_num',$stud_num);
		$this->db->where('status',1);
		$this->db->join('stud_subjects e', 'e.subj_code = s.subj_code', 'right');
		$query=$this->db->get('subjects s'); 
    	return $query->result();
		
	}

	function get_grades($stud_num)
	{
		$this->db->where('stud_num',$stud_num);
		$this->db->where('status','1');
		$this->db->join('stud_subjects e', 'e.subj_code = s.subj_code', 'right');
		$query=$this->db->get('subjects s'); 
     	return $query->result();
		
	
	}


	function get_not_courses($stud_num){
		$this->db->where('stud_num',$stud_num);
		$this->db->where('status',0);
		$this->db->join('stud_subjects e', 'e.subj_code = s.subj_code', 'right');
		$query=$this->db->get('subjects s'); 
     	return $query->result();
	}
	

	function update_courses($enroll_id){
		$data = array(
           'status' => 1
        );
        $this->db->where('enroll_id', $enroll_id);
        $query = $this->db->update('stud_subjects', $data); 
        return $query;
	}
}
