<?php

class Superadmin_model extends CI_Model{


	function check_account_db(){
        $this->db->where('admin_num', $this->input->post('myusername'));
        $this->db->where('admin_pass',$this->input->post('mypassword'));
        $query = $this->db->get('users');
		if($query->num_rows == 1)
		{
                $row = $query->row();
                $data = array(
                'admin_id' => $row->id,
                'admin_num' => $row->admin_num,
                'is_logged_in' => true
                );
                $this->session->set_userdata($data);
                return true;
		}
		else{
                return false;	
		}	   
    }

	function get_students()
	{

		$query = $this->db->get('studinfo');
		
		return $query->result();
	
	}


	function get_courses()
	
	{
		
		$query = $this->db->get('subjects');
		
		return $query->result();
	
	}


	function get_profs()
	{

		$query = $this->db->get('profinfo');
		
		return $query->result();
	
	}

	function get_studsub()
	{

		$this->db->join('stud_subjects e', 'e.subj_code = s.subj_code', 'right');
		$query=$this->db->get('subjects s'); 
        if($query->num_rows() >0){
			foreach($query->result() as $row){
			$data[]=$row;	
			}
		}
		return $data;
		
		return $query->result();
	
	}


	
	function add_students($data)
	
	{
	
		$this->db->insert('studinfo', $data);
		return;
	
	}


	function add_profs($data)
	
	{
	
		$this->db->insert('profinfo', $data);
		return;
	
	}


	function add_subjects($data)
	
	{
	
		$this->db->insert('subjects', $data);
		return;
	
	}

	function add_studsub($data)
	
	{
	
		$this->db->insert('stud_subjects', $data);
		return;
	
	}

	
	function update_students($stud_num, $data)
	
	{
		$this->db->where('stud_num', $stud_num);
		$this->db->update('studinfo', $data);
		
	}


	function update_profs($prof_num, $data)
	
	{
		$this->db->where('prof_num', $prof_num);
		$this->db->update('profinfo', $data);
		
	}


	function update_subjects($subj_code, $data)
	
	{
		$this->db->where('subj_code', $subj_code);
		$this->db->update('subjects', $data);
		
	}

	function update_studsub($enroll_id, $data)
	
	{
		$this->db->where('enroll_id', $enroll_id);
		$this->db->update('stud_subjects', $data);
		
	}

	
	function get_studentsbyid($stud_num)
	
	{
		$this->db->where('stud_num',$stud_num);
		$query = $this->db->get('studinfo');
		return $query->row();
	
	}


	function get_profsbyid($prof_num)
	
	{
		$this->db->where('prof_num',$prof_num);
		$query = $this->db->get('profinfo');
		return $query->row();
	
	}

	function get_subjbyid($subj_code)
	
	{
		$this->db->where('subj_code',$subj_code);
		$query = $this->db->get('subjects');
		return $query->row();
	
	}

	function get_studsubjbyid($enroll_id)
	
	{
		$this->db->where('enroll_id',$enroll_id);
		$query = $this->db->get('stud_subjects');
		return $query->row();
	
	}

}
