<?php

 class Studprofile extends CI_Controller{
 
	public function __construct(){
	    parent::__construct();
		$this->load->helper('url');
        $this->load->library('session');  
        $this->load->helper('form'); 
        $this->is_logged_in();
	}

	function is_logged_in(){
        $is_logged_in = $this->session->userdata('is_logged_in');
        
        if(!isset($is_logged_in) || $is_logged_in != true){
            echo 'Sorry, you don\'t have permission to access this page.';
            echo anchor('login', 'Login');
            die();  
        }
        else{
            return true;
        }   
    }

    function login_error(){
        echo 'error';
    }

    function validate_account()
    {
    	$this->load->library('form_validation');	
        $this->form_validation->set_rules('uname', 'Username', 'trim|required|callback_check_account|xss_clean');
        $this->form_validation->set_rules('password', 'Password','required');
            if($this->form_validation->run() == FALSE){
                $this->login_error();								
            }
            else{
                $data = array(
                    'myusername' => $this->input->post('myusername'),
                    'is_logged_in' => true, 
                );  
                $this->session->set_userdata($data);
                redirect('blackboard/studprofile');  			
            }
    }

    function check_account()
    {
    	 $this->load->model('studprofile_model');
            $query = $this->studprofile_model->check_account_db();
            if($query){
        		return true;
            }
            else{
        		return false;
            }
    }

	function index(){
		// $data['main_content'] = 'main';
		// $this->load->view('includes/template',$data);
		//$this->validate_account();
        $stud_num = $this->session->userdata('stud_num');
		$this->load->model('studprofile_model');
		$data= array();
        //get enrolled courses
		if($query = $this->studprofile_model->get_enrolled_courses($stud_num)){				
				$data['records'] = $query;
		}
        //get student profile
		if($query = $this->studprofile_model->get_profile($stud_num)){				
				$data['query'] = $query;
		}
        //get grades
        if($query = $this->studprofile_model->get_grades($stud_num)){             
                $data['query2'] = $query;
        }		
        //get not enrolled courses
        if($query = $this->studprofile_model->get_not_courses($stud_num)){              
                $data['records_not'] = $query;
        }       

		$data['main_content'] = 'studprofile';
		$this->load->view('includes/template',$data);
	}

	function enroll(){
		$this->load->model("studprofile_model");
		$enroll_id = $this->uri->segment(3);
        $data['records'] = $this->studprofile_model->update_courses($enroll_id);
        $data['recordsenrolled'] = $this->studprofile_model->get_enrolled_courses($enroll_id);

        $stud_num = $this->session->userdata('stud_num');
        $this->load->model('studprofile_model');
        $data= array();
        //get enrolled courses
        if($query = $this->studprofile_model->get_enrolled_courses($stud_num)){             
                $data['records'] = $query;
        }
        //get student profile
        if($query = $this->studprofile_model->get_profile($stud_num)){              
                $data['query'] = $query;
        }       
        //get not enrolled courses
        if($query = $this->studprofile_model->get_not_courses($stud_num)){              
                $data['records_not'] = $query;
        }
        //get grades
        if($query = $this->studprofile_model->get_grades($stud_num)){             
                $data['query2'] = $query;
        }        


		$data['main_content'] = 'enrolled';
		$this->load->view('includes/template',$data);
	}
}
	