<?php

class Studlogin extends CI_Controller{
	
	
	public function __construct(){
	    parent::__construct();
		$this->load->helper('url');
        $this->load->library('session');  
        $this->load->helper('form'); 
         //$this->load->library('../controllers/studprofile');
	}

	function is_logged_in(){
        $is_logged_in = $this->session->userdata('is_logged_in');
        
        if(!isset($is_logged_in) || $is_logged_in != true){
            echo 'Sorry, you don\'t have permission to access this page.';
            echo anchor('login', 'Login');
            die();  
        }
        else{
            return true;
        }   
    }

    function index()
	{
		$data['main_content'] = 'stud_login';
		$this->load->view('includes/template',$data);
	}

    function login_error(){
        $data['main_content'] = 'stud_login_error';
        $this->load->view('includes/template',$data);
    }
	

    function validate_account()
    {
    	$this->load->library('form_validation');	
        $this->form_validation->set_rules('myusername', 'Username', 'trim|numeric|required|callback_check_account|xss_clean');
        $this->form_validation->set_rules('mypassword', 'Password','required');
            if($this->form_validation->run() == FALSE){
                $this->login_error();								
            }
            else{
                $data = array(
                    'stud_num' => $this->input->post('myusername'),
                    'is_logged_in' => true, 
                );  
                $this->session->set_userdata($data);
                redirect('studprofile');  	
                // $this->studprofile->index();		
            }
    }

    function check_account()
    {
    	 $this->load->model('studprofile_model');
            $query = $this->studprofile_model->check_account_db();
            if($query){
        		return true;
            }
            else{
        		return false;
            }
    }

    function logout()
    {
        $this->session->sess_destroy();
        $this->index();
    }




}

