<?php

 class Home extends CI_Controller{
 
	public function __construct(){
	    parent::__construct();
		
		if(!$this->session->userdata('myusername'))
		{
			redirect('main');
		}
	}

	function index(){
		$this->load->view('home');
	}
}
