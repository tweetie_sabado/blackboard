<?php

class Main extends CI_Controller{
	
	public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('myusername'))
		{
			redirect('studprofile');
		}
        
        if ($this->input->post())
        {
			
			$this->checklogin();
        }
        else 
        {


		}
    
    }
    
	function index()
	{
		$data['main_content'] = 'main';
		$this->load->view('includes/template',$data);
	}
	
	function checklogin()
	{	
		$this->load->model('admins');	
		$query = $this->admins->validate();
		if($query)
		{		
				$data= array(
					'myusername' => $this->input->post('myusername'),
					'is_logged_in' => true
					);
					$this->session->set_userdata($data);
					redirect ('studprofile');
				}		
		else
		{
			redirect('site/admins_area');
		}
	}
}

