<?php

 class Profprofile extends CI_Controller{
 
	public function __construct(){
	    parent::__construct();
		$this->load->helper('url');
        $this->load->library('session');  
        $this->load->helper('form');
        $this->load->model('profprofile_model'); 
        $this->is_logged_in();
	}

	function is_logged_in(){
        $is_logged_in = $this->session->userdata('is_logged_in');
        
        if(!isset($is_logged_in) || $is_logged_in != true){
            echo 'Sorry, you don\'t have permission to access this page.';
            echo anchor('login', 'Login');
            die();  
        }
        else{
            return true;
        }   
    }

    function validate_account()
    {
    	$this->load->library('form_validation');	
        $this->form_validation->set_rules('prof_num', 'Username', 'trim|required|callback_check_account|xss_clean');
        $this->form_validation->set_rules('prof_pass', 'Password','required');
            if($this->form_validation->run() == FALSE){
                $this->validate_account();								
            }
            else{
                $data = array(
                    'myusername' => $this->input->post('myusername'),
                    'is_logged_in' => true, 
                );  
                $this->session->set_userdata($data);
                redirect('blackboard/profprofile');  			
            }
    }

    function check_account()
    {
    	 $this->load->model('profprofile_model');
            $query = $this->profprofile_model->check_account_db();
            if($query){
        		return true;
            }
            else{
        		return false;
            }
    }

	function index(){
        $data= array();
	    $prof_num = $this->session->userdata('prof_num');
        $this->load->model('profprofile_model');
        //get prof profile
        $data['query'] = $this->profprofile_model->get_profile($prof_num); 
        $data['records'] = $this->profprofile_model->get_courses($prof_num);

 
        if($query = $this->profprofile_model->get_students($prof_num)){   
            $data['rec'] = $query;
        }
      
        $data['main_content'] = 'profprofile';
        $this->load->view('includes/template',$data);
	}

    function updateGrade(){
        $this->load->model('profprofile_model');
        $data = array(

            'grade' => $this->input->post('grade')
    
        );
    
        $this->profprofile_model->update_grade($this->input->post('enroll_id'), $data);
        redirect ('profprofile');
    }


    function retGrade(){
        $data = $this->profprofile_model->get_grade($this->input->post('enroll_id'));
        echo json_encode($data);
    }

	// function enroll(){
	// 	$this->load->model("studprofile_model");
	// 	$subj_id = $this->uri->segment(3);
	// 	if ($query = $this->studprofile_model->update_courses($subj_id)){
	// 		$data['records'] = $query;
	// 	}
	// 	$data['main_content'] = 'studprofile';
	// 	$this->load->view('includes/template',$data);
	// }
}
	