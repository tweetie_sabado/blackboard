<?php

class Login extends CI_Controller{
	
	
	public function __construct(){
	    parent::__construct();
		$this->load->helper('url');
        $this->load->library('session');  
        $this->load->helper('form'); 
	}

    function index()
	{
		$data['main_content'] = 'superlogin';
		$this->load->view('includes/template',$data);
	}

    function validate_account(){ 
        $this->form_validation->set_rules('myusername', 'Username', 'trim|required|callback_check_account|xss_clean');
        $this->form_validation->set_rules('mypassword', 'Password','required');
            if($this->form_validation->run() == FALSE){
               echo 'error';                            
            }
            else{
                $data = array(
                    'admin_num' => $this->input->post('myusername'),
                    'is_logged_in' => true, 
                );  
                $this->session->set_userdata($data);
                redirect('superadmin');            
            }
    }

    function check_account()
    {
         $this->load->model('superadmin_model');
            $query = $this->superadmin_model->check_account_db();
            if($query){
                return true;
            }
            else{
                return false;
            }
    }

    function logout()
    {   
        $data = array(
                    'is_logged_in' => false, 
                ); 
        $this->session->set_userdata($data);
        $this->session->sess_destroy();
        $this->index();

    }



}

