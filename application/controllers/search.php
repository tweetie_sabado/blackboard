<?php

 class Search extends CI_Controller{
 
        public function __construct()
        {
        parent::__construct();
        $this->load->model('search_model');
        if(!$this->session->userdata('myusername'))
        {
     
        }
    }
    function index()
        {
        
            $data['main_content'] = 'search';
            $this->load->view('includes/template',$data);
        
        }

    function search_keyword()
        {
            $keyword    =   $this->input->post('keyword');
            $data['results']    =   $this->search_model->search($keyword);
            
            $data['main_content'] = 'search';
            $this->load->view('includes/template',$data);
        }


}
