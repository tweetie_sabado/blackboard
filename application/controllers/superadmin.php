<?php

 class Superadmin extends CI_Controller{
 
	public function __construct(){
	    parent::__construct();
		$this->load->model('superadmin_model');
		$this->is_logged_in();
	}
	
	function is_logged_in(){
        $is_logged_in = $this->session->userdata('is_logged_in');
        
        if(!isset($is_logged_in) || $is_logged_in != true){
            echo 'Sorry, you don\'t have permission to access this page.';
            echo anchor('login', 'Login');
            die();  
        }
        else{
            return true;
        }   
    }

	function index(){
		$data= array();
		if($query = $this->superadmin_model->get_students()){	
			$data['query1'] = $query;
		}

		if($query = $this->superadmin_model->get_profs()){
			$data['query2'] = $query;
		}		


		if($query = $this->superadmin_model->get_courses()){
			$data['query3'] = $query;
		}

		if($query = $this->superadmin_model->get_studsub()){
			$data['query4'] = $query;
		}	

		$data['main_content'] = 'admin';
		$this->load->view('includes/template',$data);
	}

	function addStudent(){
		$data = array(
			'stud_num' => $this->input->post('studnum'),
			'password' => $this->input->post('spassword'),
			'fname' => $this->input->post('sfname'),
			'lname' => $this->input->post('slname'),
			'bdate' => $this->input->post('sbdate'),
			'address' => $this->input->post('saddress'),
			'cnum' => $this->input->post('scnum'),
		
		);
		
		$this->superadmin_model->add_students($data);
		redirect ('superadmin');
	}

	function updateStudent(){
		$data = array(
			'fname' => $this->input->post('sfname'),
			'lname' => $this->input->post('slname'),
			'bdate' => $this->input->post('sbdate'),
			'address' => $this->input->post('saddress'),
			'cnum' => $this->input->post('scnum'),
			'password' => $this->input->post('spassword'),
	
		);
	
		$this->superadmin_model->update_students($this->input->post('stud_num'), $data);
		redirect ('superadmin');
	}

	function addProf()
	{
		$data = array(
			'prof_num' => $this->input->post('profnum'),
			'prof_pass' => $this->input->post('ppassword'),
			'fname' => $this->input->post('pfname'),
			'lname' => $this->input->post('plname'),
			'bdate' => $this->input->post('pbdate'),
			'address' => $this->input->post('paddress'),
			'cnum' => $this->input->post('pcnum'),
			
		);
		
		$this->superadmin_model->add_profs($data);
		redirect ('superadmin');
	}

	function updateProf(){
		$data = array(
			'fname' => $this->input->post('pfname'),
			'lname' => $this->input->post('plname'),
			'bdate' => $this->input->post('pbdate'),
			'address' => $this->input->post('paddress'),
			'cnum' => $this->input->post('pcnum'),
			'prof_pass' => $this->input->post('ppassword'),
		);
	
		$this->superadmin_model->update_profs($this->input->post('prof_num'), $data);
		redirect ('superadmin');
	}

	function addSubj(){
		$data = array(
			'subj_code' => $this->input->post('subjcode'),
			'description' => $this->input->post('name'),
			'units' => $this->input->post('units'),
			'prof_num' => $this->input->post('cprofnum'),
			'room' => $this->input->post('room'),
			'sched' => $this->input->post('sched'),
		
		);
		
		$this->superadmin_model->add_subjects($data);
		redirect ('superadmin');
	}

	function updateSubj(){
		$data = array(
			'description' => $this->input->post('name'),
			'units' => $this->input->post('units'),
			'prof_num' => $this->input->post('cprofnum'),
			'room' => $this->input->post('room'),
			'sched' => $this->input->post('sched'),
	
		);
	
		$this->superadmin_model->update_subjects($this->input->post('subj_code'), $data);
		redirect ('superadmin');
	}

	function addStudsubj(){
		$data = array(
			'stud_num' => $this->input->post('ssstudnum'),
			'subj_code' => $this->input->post('sssubjcode'),
		
		);
		
		$this->superadmin_model->add_studsub($data);
		redirect ('superadmin');
	}

	function updateStudsub(){
		$data = array(
			'stud_num' => $this->input->post('ssstudnum'),
			'subj_code' => $this->input->post('sssubjcode'),
			'status' => $this->input->post('ssstatus'),
	
		);
	
		$this->superadmin_model->update_studsub($this->input->post('enroll_id'), $data);
		redirect ('superadmin');
	}

	function retStud(){
	
		$data = $this->superadmin_model->get_studentsbyid($this->input->post('stud_num'));
		echo json_encode($data);
	}

	function retProf(){
		$data = $this->superadmin_model->get_profsbyid($this->input->post('prof_num'));
		echo json_encode($data);
	}

	function retSubj(){
		$data = $this->superadmin_model->get_subjbyid($this->input->post('subj_code'));
		echo json_encode($data);
	}

	function retStudsub(){
		$data = $this->superadmin_model->get_studsubjbyid($this->input->post('enroll_id'));
		echo json_encode($data);
	}
}
